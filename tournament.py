"""
Program for drawing tournament brackets
Licence: GNU General Public Licence

TODO: Some GUI
"""


from treestournament import TournamentDraw

import random_names as rn

randnames = rn.generate_names(22)

rn.write_file("contestants.txt", randnames)

t = TournamentDraw()

t.import_contestants("test_data/test_data.json", "INDIVIDUAL KUMITE", 'YOUTH')

t.draw_contestants()

t.generate_tree()

#t.write_files()

t.display_brackets()
