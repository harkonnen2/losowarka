"""
Classes for managing people
"""


class Person:
    """
    Class defining a person
    Attributes: string first_name
                string last_name
    """

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


class Contestant(Person):
    """Class defining contestant
        Attributes: string first_name
                    string last_name
                    string club_name
    """

    def __init__(self, first_name='bye', last_name='bye', club_name=None):
        super(Contestant, self).__init__(first_name, last_name)
        self.club_name = club_name

    def __str__(self):
        if self.first_name == 'bye' and self.last_name == 'bye':
            full_name = 'Bye\n'
        else:
            full_name = (self.first_name +
                        " " +
                        self.last_name +
                        "\n " +
                        self.club_name)
        return full_name
