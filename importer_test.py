import unittest

from importer import Importer, FileTypeError, CompetitionTypeError

class TestImporter(unittest.TestCase):
    """
    Testing file importer class
    """

    #def test_json_file(self):
    #    filename = "test_data/test_data.json"
    #    imp = Importer(filename)
    #    contestants = imp.get_contestants()

    #    self.assertIn("json", contestants)

    def test_xml_file(self):
        filename = "kontestanci.xml"
        imp = Importer(filename)
        contestants = imp.get_contestants()

        self.assertIn("xml", contestants)

    def test_xls_file(self):
        filename = "kontestanci.xls"
        imp = Importer(filename)
        contestants = imp.get_contestants()

        self.assertIn("xls", contestants)

    def test_txt_file(self):
        filename = "kontestanci.txt"
        imp = Importer(filename)
        contestants = imp.get_contestants()

        self.assertIn("textcsv", contestants)

    def test_csv_file(self):
        filename = "kontestanci.csv"
        imp = Importer(filename)
        contestants = imp.get_contestants()

        self.assertIn("textcsv", contestants)

    def test_wrong_file_type(self):
        filename = "kontestanci.gif"
        imp = Importer(filename)

        with self.assertRaises(FileTypeError):
            imp.get_contestants()

    def test_set_competition(self):
        filename = "test_data/test_data.json"
        competition = "individual kumite"
        imp = Importer(filename)

        imp.set_competition(competition)
        self.assertEqual("individual kumite", imp.competition)

    def test_set_competition_exception(self):
        filename = "test_data/test_data.json"
        competition = "invidual kumte"
        imp = Importer(filename)

        with self.assertRaises(CompetitionTypeError):
            imp.set_competition(competition)

unittest.main()
