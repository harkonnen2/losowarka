from bout import Bout

class Round:
    """
    Class representing tournament Round
    attributes: bouts - list of bouts in round type Bout
                number - number of round
                number_of_bouts - number of bouts in round
    """
    def __init__(self, number):
        """
        Initializer
        arguments: int number - the number of round
        """
        self.number = number
        self.bouts = []
        self.number_of_bouts = 0

    def add_bout(self, bout):
        """
        Adding bouts to the round.
        Arguments: Bout bout - pointer to the bout instance
        """
        self.bouts.append(bout)

    def set_number_of_bouts(self, number_of_bouts):
        """
        Store number of bouts in the round
        arguments: int number_of_bouts - quite self explanatory
        """
        self.number_of_bouts = number_of_bouts
