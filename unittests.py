import unittest

from bout import Bout
#from round import Round


class TestBoutClass(unittest.TestCase):
    """Test class bout"""
    
    def test_basic_names(self):
        aka = "Jan Kobyła"
        shiro = "Roman Konieczko"
    
        bout = Bout(aka, shiro)
        self.assertEqual("Jan Kobyła", bout.aka)
        self.assertEqual("Roman Konieczko", bout.shiro)
        self.assertEqual(None, bout.winner)
    
    def test_aka_is_bye(self):      
        aka = "bye"
        shiro = "Roman Konieczko"
        
        bout = Bout(aka, shiro)
        self.assertEqual("bye", bout.aka)
        self.assertEqual("Roman Konieczko", bout.shiro)
        self.assertEqual("Roman Konieczko", bout.winner)
    
    def test_shiro_is_bye(self):
        aka = "Jan Kobyła"
        shiro = "bye"
        
        bout = Bout(aka, shiro)
        self.assertEqual("Jan Kobyła", bout.aka)
        self.assertEqual("bye", bout.shiro)
        self.assertEqual("Jan Kobyła", bout.winner)
    
    def test_both_byes(self):
        aka = "bye"
        shiro = "bye"

unittest.main()
