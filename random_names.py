"""
Module for generating random names
Functions: write_file(filename, names) - write the text file with generated
names
        generate_names(length) - generate the list with random names
"""


from random import randint


def write_file(filename, names):
    names_file = open(filename, 'w')
    for name in names:
        names_file.write(name + "\n")
    names_file.close()


def generate_names(length):
    """
    Generate list with random names
    Arguments: int length - number of names (length of the list) to generate
    """
    surnames = ["Kowalski",
                "Nowak",
                "Petru",
                "Krzemionka",
                "Kamiński",
                "Giertych",
                "Miller",
                "Zandberg",
                "Michnik",
                "Maklakiewicz",
                "Polański",
                "Ochódzki",
                "Dyrman",
                "Hochwander",
                "Wronka",
                "Parafianowicz",
                "Mikulski",
                "Targoński",
                "Smith",
                "Jabłkowski"]

    firstnames = ["Jan",
                  "Piotr",
                  "Marcin",
                  "Krzysztof",
                  "Ryszard",
                  "Roman",
                  "Jarosław",
                  "Radosław",
                  "Lech",
                  "Rydygier",
                  "Eustachy",
                  "Brajan",
                  "Gwidon",
                  "Józef",
                  "Mikołaj",
                  "Maksymilian",
                  "Stefan",
                  "Bogusław",
                  "Gniewomir",
                  "Marian"]
    slength = len(surnames)
    flength = len(firstnames)

    names = []

    for i in range(length):
        fname = firstnames[randint(0, flength - 1)]
        sname = surnames[randint(0, slength - 1)]
        names.append(fname + " " + sname)

    return names