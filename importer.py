import json

from people import Contestant

class FileTypeError(Exception):
    def __init__(self, message):
        super(FileTypeError, self).__init__(message)


class CompetitionTypeError(Exception):
    def __init__(self, message):
        super(CompetitionTypeError, self).__init__(message)


class Importer:
    """
    class with various importers
    """

    def __init__(self, filename):
        self.contestants = []
        self.filename = filename

    def set_competition(self, competition):
        """Set the competition for which the list must be imported
        arguments: string competition
           Possible values for competition: INDIVIDUAL KUMITE,
                                            TEAM KUMITE,
                                            INDIVIDUAL KATA,
                                            TEAM KATA,
        TODO: check if argument is in possible values list if not throw an
        exception
        """
        competitions = ["INDIVIDUAL KUMITE",
                        "TEAM KUMITE",
                        "INDIVIDUAL KATA",
                        "TEAM KATA"
                        ]

        if competition.upper() in competitions:
            self.competition = competition
        else:
            raise CompetitionTypeError("Wrong competition type!")

    def set_age_category(self, age):
        """Set the age category for which the list must be imported
            Possible values for age: YOUTH, CADET, JUNIOR, SENIOR
        """
        self.age_category = age

    def import_from_json(self):
        """
        Import contestants from json file.
        Contestants file should be formatted with keys: first_name,
        last_name, club_name
        WIP: rewriting the function to be able to import list for competition
        """
        with open(self.filename) as contestants_data:
            entries = json.load(contestants_data)
            for competition in entries:
                if ((competition['name'] == self.competition) and
                    (competition['age_category'] == self.age_category)):
                    for contestant in competition['contestants']:
                        new_cont = Contestant(contestant['first_name'],
                                              contestant['last_name'],
                                              contestant['club_name'])
                        self.contestants.append(new_cont)

    def import_from_xml(self):
        """
        Import contestants from xml file
        """
        self.contestants.append("xml")

    def import_from_xls(self):
        """
        import from xls/xlsx files
        """
        self.contestants.append("xls")

    def import_from_txtcsv(self):
        """
        Import from txt/csv files
        """
        self.contestants.append("textcsv")

    def get_contestants(self):
        """
        Return list of contestants from the file
        args: filename
        """
        filetype = self.filename.split(".")
        if filetype[1] == "json":
            self.import_from_json()
        elif filetype[1] == "xml":
            self.import_from_xml()
        elif filetype[1] == "xls":
            self.import_from_xls()
        elif filetype[1] == "txt" or filetype[1] == "csv":
            self.import_from_txtcsv()
        else:
            raise FileTypeError("Unrecognized file type!")

        return self.contestants

