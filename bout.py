import logging

class Bout:
    """
    Class representing bout:
    attributes: self.aka - string, name of the aka contestant
    self.shiro - string, name of the shiro contestant
    self.winner - name of the winner (shiro or aka or Bye)
    Methods:
        set_winner - sets bout winner
    """
    def __init__(self, aka, shiro):
        self.aka = aka
        self.shiro = shiro
        # check if both contestants aren't "byes"
        try:
            if str(self.aka).upper() == "BYE" and str(self.shiro).upper() == "BYE":
                self.set_winner("Bye")
            elif str(self.aka).upper() == "BYE":  # else if aka contestant is "bye"
                # set winner shiro
                self.set_winner(self.shiro)
            elif str(self.shiro).upper() == "BYE":  # else if shiro is "bye"
                self.set_winner(self.aka)
            else:  # if no contestant is "Bye" or there are no contestants
                self.set_winner(None)
        except AttributeError:
            self.set_winner(None)

    def set_winner(self, winner):
        """
        Set bout winner
        arguments: winner string with winner name or None
        """
        self.winner = winner

    def __str__(self):
        
        return_string = "Aka: {aka}\nShiro: {shiro}".format(aka=self.aka, shiro=self.shiro)
        return_string = return_string.replace("None", "")
        return return_string
