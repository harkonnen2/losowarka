import math as m

from random import shuffle
# import plotly.plotly as pl

from bout import Bout

from round import Round

from importer import Importer

from people import Contestant


class TournamentDraw:
    """
    Class modelling tournament drawing
    attributes: contestants - list of contestants
                rounds - list of rounds
                num_rounds - number of rounds
    """
    def __init__(self):
        self.contestants = []
        self.rounds = []
        self.num_rounds = 0
        
    def import_contestants(self, filename, competition, age_category):
        """
        Import contestants from file.
        Arguments: filename - name of the file to import
        TODO: import from different formats (json, xml, xlsx etc.)
        """
        imp = Importer(filename)
        imp.set_competition(competition)
        imp.set_age_category(age_category)
        self.contestants = imp.get_contestants()
        

    def draw_contestants(self):
        """
        Shuffle the contestants list, calculate number of rounds
        """
        #check the length of the contestants list and calculate max length
        length_index = m.log(len(self.contestants), 2)
        max_length = 2**m.ceil(length_index)

        #fill the list with "Byes"
        for i in range(max_length - len(self.contestants)):
            bye=Contestant()
            self.contestants.append(bye)
        
        shuffle(self.contestants)

        num_cont = len(self.contestants)
        self.num_rounds = int(m.log(num_cont, 2))

    def generate_tree(self):
        """
        Generate tree for the tournament
        """

        # set rounds list
        for i in range(0, self.num_rounds):
            nround = Round(i + 1)  # create Round instance

            # calculate and set number of bouts in round
            num_bouts = int(len(self.contestants) / (2 ** (i + 1)))
            nround.set_number_of_bouts(num_bouts)
            if i == 0:  # if it is a first round, fill with contestants
                for c_num in range(0, len(self.contestants), 2):
                    try:
                        bout = Bout(self.contestants[c_num],
                                    self.contestants[c_num + 1])
                    except AssertionError:
                        print('Unhandled nones!')
                    nround.add_bout(bout)
            else:  # this is second round and so on
                for c_num in range(0, self.rounds[i - 1].number_of_bouts, 2):
                    try:
                        bout = Bout(self.rounds[i - 1].bouts[c_num].winner,
                                    self.rounds[i - 1].bouts[c_num + 1].winner)
                    except AssertionError:
                        print('unhandled Nones!')
                    nround.add_bout(bout)
            self.rounds.append(nround)

    def write_files(self):
        """
        Write files containing tree.
        TODO: code for different formats: SVG, PDF, HTML, graphviz etc.
        """
        for nround in self.rounds:
            print("Round: ", nround.number)
            print("*" * 45)
            filename = 'round_' + str(nround.number) + ".txt"
            roundfile = open(filename, 'w')
            for bout in nround.bouts:
                roundfile.write("\u250C"+"\u2500" * 44 + "\u2510")
                roundfile.write("\n\u2502 Aka: " + str(bout.aka) + " "*(38 - len(str(bout.aka))) + "\u2502")
                roundfile.write("\n\u251C" + "\u2500" * 44 + "\u2524")
                roundfile.write("\n\u2502 Shiro: " + str(bout.shiro) + " "*(36 - len(str(bout.shiro))) + "\u2502")
                roundfile.write("\n\u2514" + "\u2500" * 44 + "\u2518\n")
            roundfile.close()
    
    def display_brackets(self):
        pass
                        
